    /**
 * Slick Carousel
 */

$(function(){
    //console.log("CAROUSEL!");
    $('.responsive').slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: true
    });

    $('.videos').slick({
        dots: false,
        infinite: false,
        speed: 700,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: true
    });

    $('.singleItem').slick({
        dots: true,
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
    });

    $('.lgbanners').slick({
        dots: true,
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
    });

    //Custom Prev / Next Buttons and Dots
    $(".prev").on("click", function(){
        $(this).parents().prev(".slider").slickPrev();
    });

    $(".next").on("click", function(){
        $(this).parents().prev(".slider").slickNext();
    });

    $('.custom').each(function(e) {
        $dots = $(this).children(".slick-dots");
        $controls = $(this).next(".customControls").children(".dotme");
        $dots.appendTo($controls);
        //console.log($controls.html());
    });

    //Grab the large banners in the slider and place them as background images
    $('.slider .banner').each(function(){
        var imageType = $('.asBg', this).attr('src');
        $(this).css({
            'background-image' : 'url(' + imageType + ')',
            'background-repeat' : 'no-repeat',
            'background-size' : 'cover',
            'background-position' : 'center center'
        });



//        var imageType = $('.asBg', this).attr('src');
//        $(this).css({
//            'background-image' : 'url(' + imageType + ')',
//            //'background-repeat' : 'no-repeat',
//            'background-size' : '100% auto',
//            'background-repeat' : 'no-repeat',
//            'background-position' : 'center center'
//        });
    });

});