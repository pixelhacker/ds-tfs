/**
 * Desert Schools Main JS file
 */

//Fix for browsers (IE) with no console
if(typeof console === "undefined"){
    var console = {};
    console.log = function(){
        return false;
    }
}

$(function() {
    $(".megamenu").megamenu();

    /**
     * Sticky Top Nav
     */
    $("#sticky-nav").sticky({
        topSpacing: 0
    });

    //to show and hide icons when navigation is fixed to the top
    $('#sticky-nav').on('sticky-start', function() {
        $('.hidden-item', this).css('display', 'inline-block');
    });
    $('#sticky-nav').on('sticky-end', function() {
        $('.hidden-item', this).css('display', 'none');
    });

    //Change positioning of megamenu dropdowns if nav is sticky
    var $stickywrap = $(".sticky-wrapper"),
        $window = $(window);

    $window.scroll(function() {
        if($stickywrap.hasClass("is-sticky")){
            $("nav").addClass("locked");
        }else {
            $("nav").removeClass("locked");
        }
    });


    //Selected Section Arrow
    $('#section-nav li a').click(function(){
        $('#section-nav li').removeClass('active');
        $(this).parent().addClass('active');
        $(".selected-arrow").css("margin-left", $(this).data("iconposition"));
    });

    /**
     * Get Inline image and add it as Background Image
     */
        //Sections
    $('.module').each(function(){
        var inlineImg = $(".sectionBg", this).attr("src");

        if($(this).find("img.sectionBg").length) {
            $(this).css({
                'background-image' : 'url(' + inlineImg + ')',
                'background-repeat' : 'no-repeat',
                //'background-size' : '1600px 100%',
                'background-size' : 'cover',
                'background-position' : 'top center'
            });
        }
    });

    $(".item").hover(function(){
        $(".overlay", this).animate({
            "top": "0"
        }, 450, "easeInOutQuart");

    },function(){
        $(".overlay", this).animate({
            "top": "202"
        }, 450, "easeInQuart");
    });


    /**
     * Setup Stats Tabs
     */
    $('#stats-area div').hide();
    $('#stats-area div:first').show();
    $('#stats-area ul li:first').addClass('active');

    $('#stats-area ul li a').click(function(){
        $('#stats-area ul li').removeClass('active');
        $(this).parent().addClass('active');

        var currentTab = $(this).attr('href');
        console.log("current tab : " + currentTab);
        $('#stats-area div').hide();

        //grab caret position from html
        var caretPos = $(this).parent().data("position");
        console.log("car : " + caretPos);

        //set caret position
        $('.grey-line img').css('padding-top', caretPos+"px");

        $(currentTab).show();
        return false;
    });

    var item = $(".gridWrap .item");
    $(item).each(function(){
        console.log("item found");
        $(this).find('.item:nth-child(2n+1)').after('<div class="cleaner"></div>');
        //$(this).find('.gridra.item').after('<div class="cleaner"></div>');
        //$(this).find('.item:nth-child(3n+3)').after('<div class="cleaner"></div>');
        //$(this).find(".item:nth-child(4n+1)").addClass("first");
    });

    if ($(".sidebar").length > 0) {
        var first = $(".sidebar .side-title").first();
        if (first.hasClass("default")) {
            first.addClass("top");
        } else {
            first.addClass("first");
        }
    }
});