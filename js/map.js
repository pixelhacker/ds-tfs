function initialize() {
    var mapCanvas = document.getElementById('mapCanvas');
    var mapOptions = {
        center: new google.maps.LatLng(33.5103797, -112.046671),
        zoom: 13,
        disableDefaultUI: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(mapCanvas, mapOptions);
    var infowindow =  new google.maps.InfoWindow({
        content: ''
    });

    $.getJSON('/js/map-points.json', function(data) {
        $.each( data.points, function(i, value) {

            var myLatlng = new google.maps.LatLng(value.lat, value.lon);

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                icon: "/img/icons/icon-map.png"
            });

            bindInfoWindow(marker, map, infowindow, value.title + "<br>" + value.address1 + "<br>" + value.address2 + "<br>" + value.phone);
        });
    });

    function bindInfoWindow(marker, map, infowindow, html) {
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.setContent(html);
            infowindow.open(map, marker);
        });
    }
}

google.maps.event.addDomListener(window, 'load', initialize);

