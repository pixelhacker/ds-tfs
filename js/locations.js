$(function() {
    var  userLoc = {};

    
    function initMap(){
        var markers = [];

        var mapOptions = {
            center: new google.maps.LatLng(33.5103797, -112.046671),
            zoom: 11,
            disableDefaultUI: true,
            panControl: true,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById('mapCanvas'), mapOptions);
        
        var infowindow = new google.maps.InfoWindow({
            content: ''
        });

        function updateDetailPanel(detailData){
            $.get('/includes/detail-info.html', function(template, textStatus, jqXhr) {
                var result = Mustache.render($(template).filter('#detailTpl').html(), detailData);
                $('.detailed-view').html(result);

                $(".close-btn a").on("click", function (evt) {
                    evt.preventDefault();
                    $('.detailed-view').animate({'margin-top': '0px'}, 450);
                });
            });
        }

        function showBranchInfo(loc){
            updateDetailPanel(loc);
            $('.detailed-view').animate({'margin-top': '-690px'}, 650);
        }

        $.getJSON("/js/locations.json", {}, function(branchData, textStatus, jqXHr) {
            var branchList = $('.listitems');
            
            $.get('/includes/branch-info.html', function(template, textStatus, jqXhr) {
                var idx = 0;

                branchData.index = function(){
                    return idx++;
                };

                branchData.userLoc = function(){
                    if(userLoc.lat){
                        return userLoc.lat + "," + userLoc.lon;
                    } else {
                        return "";
                    }
                    
                }
                
                var result = Mustache.render($(template).filter('#branchTpl').html(), branchData);
                
                $('.listitems').html(result);

                $(".listitems").quickPager({
                    pageSize:"3"
                });

                $(".left-links a").on("click", function (evt) {
                    evt.preventDefault();
                    var index = $(this).data("id");
                    updateDetailPanel(branchData.locations[index]);
                    $('.detailed-view').animate({'margin-top': '-690px'}, 650);
                });
            });

            var LatLngList = [];
            var bounds;

            $.each(branchData.locations, function (i, locations) {
                var myLatlng = new google.maps.LatLng(locations.Latitude, locations.Longitude);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    icon: "/img/icons/icon-map.png"
                });
                
                var info = "<div style='color:#0078bd;font-size:18px;'>" + locations.BranchName + "</div>" + locations.Address + "<br>" + locations.City + ", " + locations.State;
                
                bindInfoWindow(marker, map, infowindow, info, branchData.locations, i);

                LatLngList.push(new google.maps.LatLng(locations.Latitude, locations.Longitude));
                bounds = new google.maps.LatLngBounds();

                for (var i = 0, LtLgLen = LatLngList.length; i < LtLgLen; i++) {
                    bounds.extend(LatLngList[i]);
                }
            });

            map.fitBounds (bounds);
        });

        function bindInfoWindow(marker, map, infowindow, html, data, index) {
            google.maps.event.addListener(marker, 'click', function () {
                
                infowindow.setContent(html);
                infowindow.open(map, marker);
               // showBranchInfo(data[index]);
            });

           // google.maps.event.addListener(infowindow, 'closeclick', closeMarker);
        }

        function closeMarker(){
            $('.detailed-view').animate({'margin-top': '0px'}, 450);
        }

        var input = (document.getElementById('search-field'));
        var searchBox = new google.maps.places.SearchBox((input));

        google.maps.event.addListener(searchBox, 'places_changed', function() {
            var places = searchBox.getPlaces();

            for (var i = 0, marker; marker = markers[i]; i++) {
                marker.setMap(null);
            }

            markers = [];
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0, place; place = places[i]; i++) {

                // Create a marker for each place.
                var marker = new google.maps.Marker({
                    map: map,
                    icon: "/img/icons/icon-map.png",
                    position: place.geometry.location
                });

                markers.push(marker);

                bounds.extend(place.geometry.location);
            }

            map.fitBounds(bounds);
        });

        google.maps.event.addListener(map, 'bounds_changed', function() {
            var bounds = map.getBounds();
            searchBox.setBounds(bounds);
        });
    }

    function getUserLocation(){
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                userLoc.lat = position.coords.latitude;
                userLoc.lon = position.coords.longitude;
            });
        } else {
            userLoc.lat = "";
            userLoc.lon = "";
        }

        google.maps.event.addDomListener(window, 'load', initMap);
        
    }
    
    getUserLocation();
});