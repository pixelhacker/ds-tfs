/**
 * Extractable Accordion jQuery plugin
 *
 * Usage:
 * <div class="myAccordion">
 *     <div class="accordionButton">Button 1</div>
 *     <div class="accordionContent">Content for button 1</div>
 *     <div class="accordionButton">Button 2</div>
 *     <div class="accordionContent">Content for button 2</div>
 * </div>
 *
 * $('.myAccordion').exAccordion();
 */

(function ($) {

    $.fn.exAccordion = function() {
        return this.each(function() {
            var accordionContainer = $(this);

            var triggers = accordionContainer.find('.accordionButton'),
                accordionContent = accordionContainer.find('.accordionContent');

            /* If accordion triggers and content exist */
            if (accordionContent.length && triggers.length) {
                /* Hide Accordion Content on Load */
                $(accordionContent).hide();

                /* Accordion Trigger */
                triggers.on("click", function() {
                    var tElement = $(this);

                    /**
                     * Toggle container visibility
                     */
                    if (!tElement.hasClass('active')) {
                        /* Add "active" class to clicked item (remove from all others) */
                        triggers.removeClass("active");
                        tElement.addClass("active");

                        /* Close any open accordions and open the one that's clicked */
                        $(accordionContent).slideUp('fast').removeClass("open");
                        tElement.next().slideDown('fast').addClass("open");
                    } else {
                        /* If accordion is already open, close it */
                        tElement.removeClass("active");

                        /* Close any open accordions and open the one that's clicked */
                        tElement.next().slideUp('fast').removeClass("open");
                    }

                    return false;
                });
            }
        });
    };

}(jQuery));
