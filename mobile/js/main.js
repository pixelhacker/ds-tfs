/**
 * Accordions
 */
$(function(){
    //BACKGROUND IMAGE
    $('#main-module').each(function(){
        var inlineImg = $(".sectionBg", this).attr("src");
        if($(this).find("img.sectionBg").length) {
            $(this).css({
                'background-image' : 'url(' + inlineImg + ')',
                'background-repeat' : 'no-repeat',
                'background-size' : 'cover',
                'background-position' : 'top center'
            });
        }
    });

    $('#member-benefits').each(function(){
        var inlineImg = $(".benefitsBg", this).attr("src");
        if($(this).find("img.benefitsBg").length) {
            $(this).css({
                'background-image' : 'url(' + inlineImg + ')',
                'background-repeat' : 'no-repeat',
                'background-size' : 'cover',
                'background-position' : 'top center'
            });
        }
    });


    //SLIDING SIDE MENU
    //$.slidebars({scrollLock: false});
    $.slidebars();

    $('.exAccordion').exAccordion();
    $('.ratesAccordion').exAccordion();
    $('.categoriesAccordion').exAccordion();

    /**
     * Setup Stats Tabs
     */

    $('.stats-area div').hide();
    $('.stats-area div:first').show();
    $('.stats-area ul li:first').addClass('active');

    $('.stats-area ul li a').click(function(){
        $('.stats-area ul li').removeClass('active');
        $(this).parent().addClass('active');
        //console.log("position : " + $(this).parent().data("position"));

        //grab caret position from html
        var caretPos = $(this).parent().data("position");

        //set caret position
        $('.nav-caret').css('margin-left', caretPos+"rem");

        var currentTab = $(this).attr('href');
        $('.stats-area div').hide();

        $(currentTab).show();
        return false;
    });

    /**
     * Setup Article Tabs
     */

    $('.article-area div').hide();
    $('.article-area div:first').show();
    $('.article-area ul li:first').addClass('active');

    $('.article-area ul li a').click(function(){
        $('.article-area ul li').removeClass('active');
        $(this).parent().addClass('active');
        //console.log("position : " + $(this).parent().data("position"));

        //grab caret position from html
        var caretPos = $(this).parent().data("position");

        //set caret position
        $('.nav-caret').css('margin-left', caretPos+"rem");

        var currentTab = $(this).attr('href');
        $('.article-area div').hide();

        $(currentTab).show();
        return false;
    });
});
