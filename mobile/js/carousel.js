/**
 * Slick Carousel
 */

$(function(){
    $('.banners').slick({
        dots: true,
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
    });

    $('.testimonial').slick({
        dots: false,
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
    });

//Custom Prev / Next Buttons and Dots
    $(".prev").on("click", function(){
        $(this).parents().prev(".slider").slickPrev();
    });

    $(".next").on("click", function(){
        $(this).parents().prev(".slider").slickNext();
    });

    $('.custom').each(function(e) {
        $dots = $(this).children(".slick-dots");
        $controls = $(this).next(".customControls").children(".dotme");
        $dots.appendTo($controls);
    });
});






